// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.merge.ConcurrentMerge;
import com.deltaxml.merge.ExtensionPoint;
import com.deltaxml.mergecommon.config.FormattingOutputType;

import java.io.File;
import java.io.FileNotFoundException;

public class FormattingElements {
  public static void main(String[] args) throws Exception {
    File samplesResultFolder = new File("results/");

    // Ancestor (A) and Version (B,C,D) files
    File aVersion = new File("FEInput1-A.xml");
    File bVersion = new File("FEInput1-B.xml");
    File cVersion = new File("FEInput1-C.xml");
    File dVersion = new File("FEInput1-D.xml");

    // Result File
    File resultFile = new File("result.xml");
    
    System.out.println("Running Concurrent Merge....");
    ConcurrentMerge merge= new ConcurrentMerge();
    
    // Specify which elements to consider as formatting
    FilterStepHelper fsh;
    fsh= merge.getFilterStepHelper();
    FilterChain fc;
    fc= fsh.newSingleStepFilterChain(new File("mark-formatting-1.xsl"), "mark-formatting");
    merge.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, fc);
     
    merge.setAncestor(aVersion, "A");
    System.out.println("Ancestor (\"A\") set");
    
    merge.addVersion(bVersion, "B");
    System.out.println("Version \"B\" added");
    
    merge.addVersion(cVersion, "C");
    System.out.println("Version \"C\" added");
    
    merge.addVersion(dVersion, "D");
    System.out.println("Version \"D\" added");

    System.out.println("Extracting overlapping milestones format output");
    merge.setFormattingOutputType(FormattingOutputType.OVERLAPPING_MILESTONES);
    merge.extractAll(new File(samplesResultFolder, "merge-result-overlapping-milestones.xml"));
    
    System.out.println("Extracting non overlapping milestones format output");
    merge.setFormattingOutputType(FormattingOutputType.NON_OVERLAPPING_MILESTONES);
    merge.extractAll(new File(samplesResultFolder, "merge-result-non-overlapping-milestones.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output");
    merge.setFormattingOutputType(FormattingOutputType.DELTA_V_2_1);
    merge.extractAll(new File(samplesResultFolder, "merge-result-deltav21.xml"));
    
    System.out.println("Extracting Content Group format output");
    merge.setFormattingOutputType(FormattingOutputType.CONTENT_GROUP);
    merge.extractAll(new File(samplesResultFolder, "merge-result-contentgroup.xml"));
    
    System.out.println("Concurrent Merge Result files created at: " + samplesResultFolder.getCanonicalPath());
    System.out.println("");
    
  }
}
