# Formatting Elements Sample

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Merge release.*    
*The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Merge-7_0_0_j/samples/sample-name`.*    

------
This illustrates how XML Merge helps with formatting elements. There are four versions of a simple document.  These might be multiple edits of the same document content over time or by different authors.     
This sample shows how XML Merge handles these changes with formatting elements both enabled and disabled.   
    
For full information about the formatting elements sample see our web page [Formatting Elements Sample](https://docs.deltaxml.com/xml-merge/latest/samples-and-guides/formatting-elements-sample)     
    
To run this sample, you will require [Apache Ant](http://ant.apache.org/) and a [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). From a command prompt in the sample directory, you can compile and run the sample with:

     ant


The following targets are supplied in build.xml:

| Target | Description |
| ------- |  --------- |
| run | Default. This depends on the compile target, and runs the main method of [FormattingElements.java](https://bitbucket.org/deltaxml/formatting-elements-sample/src/default/FormattingElements.java). |
| compile | Compiles FormattingElements.java into a Java .class file in the class directory, which can be invoked either through the java command or from the ant run target above. |
| clean | This deletes everything in the .class directory. |
